package com.transport.app.exceptions;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
public class ObjectNotFoundException extends Throwable {

    public ObjectNotFoundException(String message) {
        super(message);
    }
}
