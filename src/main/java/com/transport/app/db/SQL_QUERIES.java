package com.transport.app.db;

/**
 * Created by lulzimgashi on 13/07/2017.
 */
public class SQL_QUERIES {
    public static final String GET_TRUCKS_WITH_FILTERS = "SELECT t.*, tt.*, img.*, ROUND(COALESCE(lat_lng_distance(:latitude, :longitude, t.latitude, t.longitude), 0), 2) AS km FROM trucks t LEFT JOIN truck_types tt ON t.truck_type_id = tt.id LEFT JOIN images img ON t.id = img.truck_id WHERE IF(length(:tname) > 1, tt.name = :tname, TRUE) AND IF(length(:tmark) > 1, tt.mark = :tmark, TRUE) AND IF(length(:ttonnes) > 0, tt.tonnes = :ttonnes, TRUE) AND IF(length(:locationName) > 1, t.location_name = :locationName, TRUE) HAVING(IF(length(:longitude) > 1, km <= :km, TRUE));";
}
