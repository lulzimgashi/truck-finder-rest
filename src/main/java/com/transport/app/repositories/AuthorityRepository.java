package com.transport.app.repositories;

import com.transport.app.models.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {

    @Modifying
    @Transactional
    @Query("update Authority u SET u.username=?1 WHERE u.userId=?2 ")
    void updateAuthority(String username, String id);

    Authority findByUsername(String username);
}
