package com.transport.app.controllers;

import com.transport.app.exceptions.ObjectAlreadyExistException;
import com.transport.app.exceptions.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * Created by lulzimgashi on 06/06/2017.
 */
@RestControllerAdvice(annotations = {RestController.class})
public class MyControllerAdvice {

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<String> returnMessage(ObjectNotFoundException exception) {
        return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObjectAlreadyExistException.class)
    public ResponseEntity<String> returnMessage(ObjectAlreadyExistException exception) {
        return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
