package com.transport.app.services;

import com.transport.app.exceptions.ObjectAlreadyExistException;
import com.transport.app.exceptions.ObjectNotFoundException;
import com.transport.app.models.Authority;
import com.transport.app.models.Truck;
import com.transport.app.models.User;
import com.transport.app.repositories.AuthorityRepository;
import com.transport.app.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Service
public class UserService {

    @Autowired
    private TruckService truckService;


    private StandardPasswordEncoder standardPasswordEncoder = new StandardPasswordEncoder();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Transactional
    public User addUser(User user) throws Throwable {
        boolean exist = userRepository.checkIfExist(user.getUsername(), "123");

        if (exist) {
            throw new ObjectAlreadyExistException("User with same username exists");
        }


        user.setPassword(new StandardPasswordEncoder().encode(user.getPassword()));
        User save = userRepository.save(user);

        Authority authority = new Authority();
        authority.setUsername(save.getUsername());
        authority.setAuthority(user.getRole() != null && user.getRole().equals("ROLE_ADMIN") ? "ROLE_ADMIN" : "ROLE_USER");
        authority.setUserId(save.getId());

        authorityRepository.save(authority);


        User newUser = new User();

        BeanUtils.copyProperties(save, newUser);

        newUser.setPassword("PROTECTED");
        return newUser;
    }

    @Transactional
    public User updateUser(User user, String username) throws Throwable {
        User loggedUser = userRepository.findByUsername(username);

        if (!loggedUser.getId().equals(user.getId())) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        boolean exist = userRepository.checkIfExist(user.getUsername(), loggedUser.getId());
        if (exist) {
            throw new ObjectAlreadyExistException("User with same username exists");
        }

        User one = userRepository.findOne(user.getId());
        if (one == null) {
            throw new ObjectNotFoundException("User doesn't exists");
        }

        user.setPassword(one.getPassword());
        User save = userRepository.save(user);

        authorityRepository.updateAuthority(save.getUsername(), save.getId());

        User newUser = new User();

        BeanUtils.copyProperties(save, newUser);

        newUser.setPassword("PROTECTED");
        return newUser;

    }

    @Transactional
    public String deleteUser(String id, String username) {
        User loggedUser = userRepository.findByUsername(username);

        if (!loggedUser.getId().equals(id)) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        userRepository.dUser(id);
        return "Done";
    }

    @Transactional(readOnly = true)
    public User getUser(String userId) {
        User one = userRepository.findByIdAndEnabledTrue(userId);
        if (one != null) {
            one.setPassword("PROTECTED");
        }
        List<Truck> allForUser = truckService.getAllForUser(userId);

        one.setTruckList(allForUser);
        return one;
    }

    public String changePassword(User user, String username) throws Throwable {
        User loggedUser = userRepository.findByUsername(username);
        User dbUser = userRepository.findOne(user.getId());

        if (!loggedUser.getId().equals(dbUser.getId())) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        dbUser.setPassword(new StandardPasswordEncoder().encode(user.getPassword()));

        userRepository.save(dbUser);

        return "done";
    }

    @Transactional(readOnly = true)
    public User getLoginUser(String name, String password) {

        User byUsername = userRepository.findByUsername(name);
        Authority authority = authorityRepository.findByUsername(byUsername.getUsername());

        byUsername.setRole(authority.getAuthority());

        if (password == null) {
            byUsername.setPassword("PROTECTED");
            return byUsername;
        }

        if (this.standardPasswordEncoder.matches(password,byUsername.getPassword())) {
            byUsername.setPassword("PROTECTED");
            return byUsername;
        } else {
            throw new BadCredentialsException("Wrong Username Or Password");
        }
    }
}
