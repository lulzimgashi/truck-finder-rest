package com.transport.app.controllers;

import com.transport.app.models.TruckType;
import com.transport.app.services.TruckTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@RestController
@RequestMapping("truck_types")
public class TruckTypeController {

    @Autowired
    private TruckTypeService truckTypeService;

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseEntity<TruckType> addTruckType(@RequestBody TruckType truckType) {
        TruckType save = truckTypeService.addTruckType(truckType);

        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = {"application/json"})
    public ResponseEntity<TruckType> updateTruckType(@RequestBody TruckType truckType) {
        TruckType save = truckTypeService.updateTruckType(truckType);

        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/delete/{truckTypeId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> updateTruckType(@PathVariable String truckTypeId) {
        String save = truckTypeService.deleteTruckType(truckTypeId);

        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<List<TruckType>> getAll() {
        List<TruckType> save = truckTypeService.getTruckTypes();

        return new ResponseEntity<>(save, HttpStatus.OK);
    }

}
