package com.transport.app.services;

import com.transport.app.Utilities.MyFTPclient;
import com.transport.app.models.Image;
import com.transport.app.repositories.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by lulzimgashi on 07/06/2017.
 */
@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private MyFTPclient myFTPclient;

    @Value("${ftp-host}")
    private String ftp_host;

    public Image upload(InputStream is, String truckId) throws IOException {

        String uuid = UUID.randomUUID().toString();

        myFTPclient.upload(is, uuid);

        Image image = new Image();
        image.setId(uuid);
        image.setTruckId(truckId);
        image.setUrl(ftp_host + "/images/" + uuid + ".png");

        return imageRepository.save(image);
    }

    public String deleteImage(String id) throws IOException {
        Image one = imageRepository.findOne(id);

        if (one != null) {
            imageRepository.delete(id);
            myFTPclient.delete(one.getUrl());
        }
        return "done";
    }
}
