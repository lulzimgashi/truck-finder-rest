package com.transport.app.repositories;

import com.transport.app.models.TruckType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Repository
public interface TruckTypeRepository extends JpaRepository<TruckType,String>{
}
