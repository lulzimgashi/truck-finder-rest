package com.transport.app.models;

/**
 * Created by lulzimgashi on 07/06/2017.
 */
public class ImgObj {

    private String base64img;
    private String truckId;


    public String getBase64img() {
        return base64img;
    }

    public void setBase64img(String base64img) {
        this.base64img = base64img;
    }

    public String getTruckId() {
        return truckId;
    }

    public void setTruckId(String truckId) {
        this.truckId = truckId;
    }
}
