package com.transport.app.repositories;

import com.transport.app.models.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Repository
public interface TruckRepository extends JpaRepository<Truck, String> {

    List<Truck> findAllByUserId(String userId);
}
