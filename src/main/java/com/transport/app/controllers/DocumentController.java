package com.transport.app.controllers;

import com.transport.app.models.Image;
import com.transport.app.models.ImgObj;
import com.transport.app.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by lulzimgashi on 07/06/2017.
 */
@RestController
@RequestMapping("documents")
public class DocumentController {

    @Autowired
    private ImageService imageService;


    @RequestMapping(value = "/{truckId}", method = RequestMethod.POST)
    public ResponseEntity<Image> addImage(@RequestParam("file") MultipartFile file,
                                          @PathVariable String truckId) throws IOException {

        Image upload = imageService.upload(file.getInputStream(), truckId);

        return new ResponseEntity<>(upload, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> upload(@PathVariable String id) throws Throwable {
        String s = imageService.deleteImage(id);

        return new ResponseEntity<>(s, HttpStatus.OK);
    }
}
