package com.transport.app.repositories;

import com.transport.app.models.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 07/06/2017.
 */
@Repository
public interface ImageRepository extends JpaRepository<Image,String>{
}
