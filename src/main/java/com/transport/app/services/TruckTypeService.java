package com.transport.app.services;

import com.transport.app.models.TruckType;
import com.transport.app.repositories.TruckTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Service
public class TruckTypeService {

    @Autowired
    private TruckTypeRepository truckTypeRepository;


    @Transactional
    public TruckType addTruckType(TruckType truckType) {
        return truckTypeRepository.save(truckType);
    }


    @Transactional
    public TruckType updateTruckType(TruckType truckType) {
        return truckTypeRepository.save(truckType);
    }


    public List<TruckType> getTruckTypes() {
        return truckTypeRepository.findAll();
    }

    @Transactional
    public String deleteTruckType(String truckTypeId) {
        truckTypeRepository.delete(truckTypeId);

        return "done";
    }
}
