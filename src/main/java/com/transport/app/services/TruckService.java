package com.transport.app.services;

import com.transport.app.db.SQL_QUERIES;
import com.transport.app.models.Image;
import com.transport.app.models.Truck;
import com.transport.app.models.TruckType;
import com.transport.app.models.User;
import com.transport.app.repositories.TruckRepository;
import com.transport.app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@Service
public class TruckService {

    @Autowired
    private TruckRepository truckRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public Truck addNew(Truck truck, String name) {
        User user = userRepository.findByUsername(name);

        truck.setUserId(user.getId());

        return truckRepository.save(truck);
    }


    @Transactional
    public Truck updateTruck(Truck truck, String name) throws Throwable {
        User user = userRepository.findByUsername(name);

        if (!user.getId().equals(truck.getUserId())) {
            throw new AccessDeniedException("You don't have access to edit that truck");
        }

        truck.setUserId(user.getId());
        return truckRepository.save(truck);

    }

    @Transactional(readOnly = true)
    public Truck getOne(String id) {

        Truck dbTruck = jdbcTemplate.query("SELECT t.*, tt.*,img.* FROM trucks t INNER JOIN truck_types tt ON t.truck_type_id = tt.id LEFT JOIN images img ON img.truck_id=t.id WHERE t.id = ?", new Object[]{id}, new ResultSetExtractor<Truck>() {
            @Override
            public Truck extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                Truck truck = new Truck();
                while (resultSet.next()) {
                    truck.setId(resultSet.getString("t.id"));
                    truck.setDescription(resultSet.getString("description"));
                    truck.setLatitude(resultSet.getDouble("latitude"));
                    truck.setLongitude(resultSet.getDouble("longitude"));
                    truck.setUserId(resultSet.getString("user_id"));
                    truck.setTruckTypeId(resultSet.getString("truck_type_id"));
                    truck.setLocationName(resultSet.getString("location_name"));

                    TruckType truckType = new TruckType();
                    truckType.setId(resultSet.getString("tt.id"));
                    truckType.setMark(resultSet.getString("mark"));
                    truckType.setName(resultSet.getString("name"));
                    truckType.setTonnes(resultSet.getInt("tonnes"));

                    truck.setTruckType(truckType);
                    break;
                }

                resultSet.beforeFirst();

                List<Image> images = new ArrayList<>();
                while (resultSet.next()) {
                    if (resultSet.getString("img.id") != null) {
                        Image image = new Image();
                        image.setId(resultSet.getString("img.id"));
                        image.setTruckId(resultSet.getString("truck_id"));
                        image.setUrl(resultSet.getString("url"));

                        images.add(image);
                    }
                }

                truck.setImageList(images);

                return truck;
            }
        });

        return dbTruck;
    }

    @Transactional(readOnly = true)
    public List<Truck> getAllForUser(String userId) {
        List<Truck> trucks = jdbcTemplate.query("SELECT t.*, tt.*,img.* FROM trucks t INNER JOIN truck_types tt ON t.truck_type_id = tt.id LEFT JOIN images img ON t.id=img.truck_id WHERE t.user_id = ?", new Object[]{userId}, new ResultSetExtractor<List<Truck>>() {
            @Override
            public List<Truck> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                return getTrucks(resultSet);
            }
        });


        return trucks;
    }


    @Transactional
    public String deleteTruck(String truckId, String name) throws Throwable {
        User user = userRepository.findByUsername(name);
        Truck truck = truckRepository.findOne(truckId);

        if (!user.getId().equals(truck.getUserId())) {
            throw new AccessDeniedException("You don't have access to delete that truck");
        }

        truckRepository.delete(truckId);

        return "done";
    }


//    @Transactional(readOnly = true)
//    public List<Truck> getWithCoordinates(String latitude, String longitude, Integer km) {
//        List<Truck> trucks = jdbcTemplate.query("SELECT t.*, tt.*,img.*, ROUND(COALESCE(lat_lng_distance(?, ?, t.latitude, t.longitude), 0), 2) AS km\n" +
//                "FROM trucks t INNER JOIN truck_types tt ON t.truck_type_id = tt.id LEFT JOIN images img ON img.truck_id=t.id HAVING km < ? ORDER BY km;", new Object[]{latitude, longitude, km}, new ResultSetExtractor<List<Truck>>() {
//            @Override
//            public List<Truck> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
//                return getTrucks(resultSet);
//            }
//        });
//
//        return trucks;
//    }


    @Transactional(readOnly = true)
    public List<Truck> getWithFilters(String truckName, String truckMark, String truckToon, String locationName, String latitude, String longitude, Integer km) {

        Map<String, Object> params = new HashMap<>();
        params.put("tname", truckName);
        params.put("tmark", truckMark);
        params.put("ttonnes", truckToon);
        params.put("locationName", locationName);
        params.put("longitude", longitude);
        params.put("latitude", latitude);
        params.put("km", km);


        List<Truck> trucks = namedParameterJdbcTemplate.query(SQL_QUERIES.GET_TRUCKS_WITH_FILTERS, params, new ResultSetExtractor<List<Truck>>() {
            @Override
            public List<Truck> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                return getTrucks(resultSet);
            }
        });

        return trucks;
    }

    private List<Truck> getTrucks(ResultSet resultSet) throws SQLException {
        List<String> ids = new ArrayList<>();
        List<Truck> trucks = new ArrayList<>();
        while (resultSet.next()) {
            if (!ids.contains(resultSet.getString("t.id"))) {
                Truck truck = new Truck();
                truck.setId(resultSet.getString("t.id"));
                truck.setDescription(resultSet.getString("description"));
                truck.setLatitude(resultSet.getDouble("latitude"));
                truck.setLongitude(resultSet.getDouble("longitude"));
                truck.setUserId(resultSet.getString("user_id"));
                truck.setTruckTypeId(resultSet.getString("truck_type_id"));
                truck.setLocationName(resultSet.getString("location_name"));

                TruckType truckType = new TruckType();
                truckType.setId(resultSet.getString("tt.id"));
                truckType.setMark(resultSet.getString("mark"));
                truckType.setName(resultSet.getString("name"));
                truckType.setTonnes(resultSet.getInt("tonnes"));

                truck.setTruckType(truckType);

                trucks.add(truck);
                ids.add(resultSet.getString("t.id"));
            }
        }
        resultSet.beforeFirst();

        List<Image> images = new ArrayList<>();
        while (resultSet.next()) {
            if (resultSet.getString("img.id") != null) {
                Image image = new Image();
                image.setId(resultSet.getString("img.id"));
                image.setTruckId(resultSet.getString("truck_id"));
                image.setUrl(resultSet.getString("url"));

                images.add(image);
            }
        }

        for (Truck truck : trucks) {
            List<Image> truckImages = new ArrayList<>();
            for (Image image : images) {
                if (image.getTruckId().equals(truck.getId())) {
                    truckImages.add(image);
                }
            }
            truck.setImageList(truckImages);
        }

        return trucks;
    }
}
