package com.transport.app.controllers;

import com.transport.app.models.Truck;
import com.transport.app.services.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
@RestController
@RequestMapping("trucks")
public class TruckController {

    @Autowired
    private TruckService truckService;


    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<List<Truck>> getAllTruck(@RequestParam(value = "truckName", required = false) String truckName,
                                                   @RequestParam(value = "truckMark", required = false) String truckMark,
                                                   @RequestParam(value = "truckToon", required = false) String truckToon,
                                                   @RequestParam(value = "locationName", required = false) String locationName,
                                                   @RequestParam(value = "latitude", required = false) String latitude,
                                                   @RequestParam(value = "longitude", required = false) String longitude,
                                                   @RequestParam(value = "km", required = false) Integer km) {

        List<Truck> trucks;
//
//        if (latitude != null && longitude != null) {
//            trucks = truckService.getWithCoordinates(latitude, longitude, km);
//        } else {
        trucks = truckService.getWithFilters(truckName, truckMark, truckToon, locationName, latitude.length() < 1 ? "0" : latitude, longitude.length() < 1 ? "0" : longitude, km);
        //}

        return new ResponseEntity<>(trucks, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseEntity<Truck> addTruck(@RequestBody Truck truck) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Truck save = truckService.addNew(truck, authentication.getName());

        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = {"application/json"})
    public ResponseEntity<Truck> updateTruck(@RequestBody Truck truck) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Truck save = truckService.updateTruck(truck, authentication.getName());

        return new ResponseEntity<>(save, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Object> getTrucksForUser(@PathVariable String id, @RequestParam("type") String type) {
        if (type.equals("one")) {
            Truck truck = truckService.getOne(id);
            return new ResponseEntity<>(truck, HttpStatus.OK);
        } else {
            List<Truck> trucks = truckService.getAllForUser(id);
            return new ResponseEntity<>(trucks, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/delete/{truckId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTruck(@PathVariable String truckId) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String message = truckService.deleteTruck(truckId, authentication.getName());

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
