package com.transport.app.exceptions;

/**
 * Created by lulzimgashi on 06/06/2017.
 */
public class ObjectAlreadyExistException extends Throwable {

    public ObjectAlreadyExistException(String message) {
        super(message);
    }
}
